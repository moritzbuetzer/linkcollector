<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="A really simple App to collect Links.">
	<meta name="author" content="Moritz Buetzer">

    <title>Frontend-Links</title>

	<link rel="stylesheet" href="css/pure-grid-min.css">
    <link rel="stylesheet" href="css/styles.css">  

</head>
<body>

	<div id="layout" class="pure-g">
	    <div class="sidebar pure-u-1 pure-u-md-1-4">
	        <form method="post" action="submit.php" class="pure-form pure-form-stacked">			
		        <input name="href" autocomplete="off" type="text" required placeholder="Link">
		        <button type="submit" class="pure-button pure-button-primary submit-button">eintragen</button>
			</form>
	    </div>
	
	    <div class="content pure-u-1 pure-u-md-3-4">
	        <div>
	            <div class="posts">
					<?php
						include_once("includes/class.linkcollector.php");						
						$linkCollector = new LinkCollector();
						$results = $linkCollector->getRecords();
						
						while ($row = $results->fetchArray()) {
						    echo '<section class="post"><header class="post-header"><h2 class="post-title"><a target="_blank" href="'.$row['href'].'">'.$row['title'].'</a></h2></header>';
						    
				        	if( !empty($row['description']) ) {
					        	echo '<div class="post-description"><p>'.$row['description'].'</p></div>';
				        	}
				        	
				            echo '</section>';
						}
					?>
	            </div>
	        </div>
	    </div>
	</div>
	
	</body>
</html>
